<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PagesController extends Controller
{
    //This controller allows us to view the main pages.
    public function show($page)
    {
        if(view()->exists('pages.' .$page)){
            return view('pages.' . $page);
        }
        else{
            //404
            abort(404, "Page not found");
        }
    }
}
