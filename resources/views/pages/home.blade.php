@extends('templates.main')

@section('title', 'Home')

@section('content')
    <div class="container h-100">
        <div class="row align-items-center h-100">
            <div class="col-6 mx-auto">
                <div class="jumbotron text-center">
                    <h1>Welcome to {{ config( 'app.name' ) }}</h1>
                    <h2>Version: {{ config( 'app.ver' ) }}</h2>
                    <ul class="nav justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link" href="https://ecwebservices.net" target="_blank">ECWebServices</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Docs</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Tutorials</a>
                        </li>
                        <li class"name-item">
                            <a class="nav-link" href="/admin">Admin</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
